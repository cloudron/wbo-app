FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

ARG NODE_VERSION=22.9.0
RUN mkdir -p /usr/local/node-${NODE_VERSION} && \
    curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-${NODE_VERSION}
ENV PATH /usr/local/node-${NODE_VERSION}/bin:$PATH

# renovate: datasource=github-tags depName=lovasoa/whitebophir versioning=semver extractVersion=^v(?<version>.+)$
ARG WBO_VERSION=1.21.2

RUN curl -L https://github.com/lovasoa/whitebophir/archive/v${WBO_VERSION}.tar.gz | tar -xz --strip-components 1 -f -

RUN rm -rf /app/code/server-data/ && \
    ln -s /app/data/server-data /app/code/server-data

RUN chown -R cloudron:cloudron /app/code

RUN npm install --production

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
