#!/bin/bash

set -eu

mkdir -p /app/data/server-data

if [[ ! -f /app/data/env ]]; then
    echo -e "# Set custom configuration here (https://github.com/lovasoa/whitebophir/blob/master/server/configuration.js)\n\n# export WBO_MAX_SAVE_DELAY=30000\n" > /app/data/env
fi

chown -R cloudron:cloudron /app/data/

source /app/data/env

echo "==> Starting WBO"
exec gosu cloudron:cloudron npm start

